import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const baseUrl = "http://localhost:5000/api/v1";

export const fetchTodoData = createAsyncThunk(
  "todo/fetchTodoData",

  async () => {
    const { data } = await axios.get(`${baseUrl}/todo`);
    return data;
  }
);
export const sendTodoData = createAsyncThunk(
  "todo/postTodoData",
  async (data) => {
    await axios.post(`${baseUrl}/todo`, {
      taskName: data.taskName,
      description: data.description,
      status: data.status,
    });
  }
);
export const deleteTodoData = createAsyncThunk(
  "todo/deleteTodoData",
  async (id) => {
    const response = await axios.delete(`${baseUrl}/todo/${id}`);
  }
);
export const updateTodoData = createAsyncThunk(
  "todo/updateTodoData",
  async (payload, extra) => {
    await axios.patch(`${baseUrl}/todo/${payload.id}`, payload);
  }
);
export const updateTodoStatus = createAsyncThunk(
  "todo/updateTodoData",
  async (payload) => {
    await axios.patch(`${baseUrl}/todo/status/${payload.id}`, payload);
  }
);

const todoSlice = createSlice({
  name: "todoSlice",
  initialState: {
    loading: "pending",
    data: null,
    dataChange: 1,
  },
  reducers: {
    changeDataEvent: (state) => {
      state.dataChange = state.dataChange + 1;
    },
  },
  extraReducers: {
    [fetchTodoData.pending](state) {
      state.loading = "pending";
    },
    [fetchTodoData.fulfilled](state, { payload }) {
      state.loading = "fullfilled";
      state.data = payload;
    },
    [fetchTodoData.rejected](state) {
      state.loading = "rejected";
    },
  },
});

export default todoSlice.reducer;
export const todoAction = todoSlice.actions;
