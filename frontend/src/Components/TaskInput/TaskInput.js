import React, { useState, useRef } from "react";
import {
  Pane,
  Dialog,
  TextInput,
  Label,
  Textarea,
  toaster,
} from "evergreen-ui";
import { useDispatch, useSelector } from "react-redux";
import { sendTodoData, todoAction } from "../../app/todoSlice";
const TaskInput = ({ isShown, close, origin }) => {
  const [enteredTaskName, setEnteredTaskName] = useState("");
  const [enteredDes, setEnterDes] = useState("");

  const [invalidField, setInvalidField] = useState(false);
  const taskNameInputRef = useRef();
  const descriptionRef = useRef();
  const dispatch = useDispatch();
  const onBlurHandler = () => {
    if (enteredTaskName.trim() === "") {
      setInvalidField(true);
    }
  };

  const onConfirmHandler = (close) => {
    if (!taskNameInputRef.current.value) {
      toaster.danger("Please Enter Task Name", { duration: 1.5 });
      setInvalidField(true);
    } else {
      setInvalidField(false);
      toaster.success("Your Task Added", { duration: 1.5 });
      const data = {
        taskName: enteredTaskName,
        description: enteredDes || "",
        status: "pending",
      };
      dispatch(sendTodoData(data));
      dispatch(todoAction.changeDataEvent());
      close();
    }
    setEnteredTaskName("");
    setEnterDes("");
  };
  console.log(enteredTaskName);
  return (
    <Pane>
      <Dialog
        isShown={isShown}
        title="Add Todo"
        onCloseComplete={() => close(false)}
        confirmLabel="Confirm"
        onConfirm={onConfirmHandler}
      >
        <Label
          htmlFor="textarea-2"
          marginTop={10}
          marginBottom={4}
          display="block"
        >
          Add Task Name
        </Label>
        <TextInput
          placeholder="Write Task Name"
          ref={taskNameInputRef}
          isInvalid={invalidField}
          value={enteredTaskName}
          onChange={(event) => {
            setEnteredTaskName(event.target.value);
            setInvalidField(false);
          }}
          onBlur={onBlurHandler}
        />

        <Label
          htmlFor="textarea-2"
          marginTop={10}
          marginBottom={4}
          display="block"
        >
          Add Task Description
        </Label>
        <Textarea
          id="textarea-2"
          placeholder="Textarea"
          value={enteredDes}
          ref={descriptionRef}
          onChange={(e) => setEnterDes(e.target.value)}
        />
      </Dialog>
    </Pane>
  );
};

export default TaskInput;
