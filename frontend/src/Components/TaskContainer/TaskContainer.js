import React, { useState, useEffect, useCallback } from "react";
import { SegmentedControl, Spinner } from "evergreen-ui";
import "./TaskContainer.scss";
import TaskCard from "../PendingTaskCard/TaskCard";
import { useDispatch, useSelector } from "react-redux";
import { fetchTodoData } from "../../app/todoSlice";

const TaskStatus = ({ data, loading }) => {
  const [value, setValue] = useState("pending");
  const dispatch = useDispatch();
  const dataChange = useSelector((state) => state.todo.dataChange);
  const [options] = useState([
    { label: "Pending ", value: "pending" },
    { label: "Completed", value: "completed" },
    { label: "Paused", value: "paused" },
  ]);

  useEffect(() => {
    dispatch(fetchTodoData());
  }, [dataChange]);

  const clickHandler = () => {};
  return (
    <>
      <div className="task-wrapper">
        <div className="task-status">
          <SegmentedControl
            width={540}
            options={options}
            value={value}
            onClick={clickHandler}
            onChange={(value) => {
              setValue(value);
            }}
          />
        </div>
      </div>
      <div className="task-area">
        {loading === "pending" ? (
          <div className="spinner">
            <Spinner />
          </div>
        ) : (
          data.todo.map((item, index) =>
            value === item.status ? (
              <TaskCard
                value={value}
                id={item._id}
                title={item.taskName}
                description={item.description}
                key={index}
              />
            ) : null
          )
        )}
      </div>
    </>
  );
};

export default TaskStatus;
