import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteTodoData, updateTodoStatus } from "../../app/todoSlice";
import { todoAction } from "../../app/todoSlice";
import {
  Popover,
  Position,
  Menu,
  PauseIcon,
  TickIcon,
  EditIcon,
  TrashIcon,
  Button,
  Pane,
  Dialog,
  Pill,
  MaximizeIcon,
  PlusIcon,
  Label,
  TextInput,
  Textarea,
  PlayIcon,
} from "evergreen-ui";
import "./TaskCard.scss";

import UpdateTaskInput from "../UpdateTaskInput/UpdateTaskInput";
const TaskCard = (props) => {
  const [isShown, setIsShown] = useState(false);
  const [editDialog, setEditDialog] = useState(false);
  const dispatch = useDispatch();
  console.log(props);

  const deleteTask = async () => {
    await dispatch(deleteTodoData(props.id));
    dispatch(todoAction.changeDataEvent());
  };

  const editDialogHandler = (payload) => {
    setEditDialog(payload);
  };
  const changeStatus = async (status) => {
    await dispatch(updateTodoStatus({ status: status, id: props.id }));
    dispatch(todoAction.changeDataEvent());
  };

  const style = {
    background: "",
    border: "",
    color: "",
  };

  if (props.value === "pending") {
    style.background = "#ffbe0b7d";
    style.border = "1px solid #ffbe0b";
    style.color = "black";
  } else if (props.value === "completed") {
    style.background = "#90be6db8";
    style.border = "1px solid #90be6d";
    style.color = "black";
  } else {
    style.background = "#780000b0";
    style.border = "1px solid #c1121f";
    style.color = "white";
  }

  return (
    <>
      <UpdateTaskInput
        isShown={editDialog}
        close={editDialogHandler}
        data={{
          taskName: props.title,
          description: props.description,
        }}
        id={props.id}
      />
      <Pane
        className="task-column"
        style={{
          background: style.background,
          border: style.border,
          color: style.color,
        }}
      >
        <div className="task-name-wrapper">
          <h3 className="task-title">{props.title}</h3>
        </div>

        <Dialog
          className="task-dialog"
          isShown={isShown}
          title={props.title}
          onCloseComplete={() => setIsShown(false)}
          hasFooter={false}
        >
          {props.description}
        </Dialog>

        <div className="buttons">
          <Button
            appearance="primary"
            onClick={() => {
              setIsShown(true);
            }}
            style={{ padding: "0" }}
          >
            <MaximizeIcon />
          </Button>
          <Popover
            position={Position.TOP_LEFT}
            content={
              <Menu>
                <Menu.Group>
                  {props.value === "pending" && (
                    <Menu.Item
                      icon={TickIcon}
                      intent="success"
                      onClick={() => changeStatus("completed")}
                    >
                      Completed
                    </Menu.Item>
                  )}
                  {props.value === "pending" ? (
                    <Menu.Item
                      icon={PauseIcon}
                      onClick={() => changeStatus("paused")}
                    >
                      Paused
                    </Menu.Item>
                  ) : (
                    <Menu.Item
                      icon={PlayIcon}
                      onClick={() => changeStatus("pending")}
                    >
                      Pending
                    </Menu.Item>
                  )}
                  {props.value === "pending" && (
                    <Menu.Item
                      icon={EditIcon}
                      onClick={() => setEditDialog(true)}
                    >
                      Edit Task
                    </Menu.Item>
                  )}
                  <Menu.Item
                    icon={TrashIcon}
                    intent="danger"
                    onClick={deleteTask}
                  >
                    Delete Task
                  </Menu.Item>
                </Menu.Group>
              </Menu>
            }
          >
            <Button
              style={{
                marginLeft: "10px",
                background: "#2b2d42",
                color: "white",
                padding: "0",
              }}
            >
              <PlusIcon />
            </Button>
          </Popover>
        </div>
      </Pane>
    </>
  );
};

export default TaskCard;
