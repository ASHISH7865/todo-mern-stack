import React, { useState, useRef } from "react";
import {
  Pane,
  Dialog,
  TextInput,
  Label,
  Textarea,
  toaster,
  SelectMenu,
  Button,
} from "evergreen-ui";
import { useDispatch } from "react-redux";
import { updateTodoData, todoAction } from "../../app/todoSlice";

const UpdateTaskInput = ({ isShown, close, data, id }) => {
  const [enteredTaskName, setEnteredTaskName] = useState("");
  const [enteredDes, setEnterDes] = useState("");
  const [selected, setSelected] = React.useState(null);
  const [invalidField, setInvalidField] = useState(false);
  const taskNameInputRef = useRef();
  const descriptionRef = useRef();
  const dispatch = useDispatch();
  const onBlurHandler = () => {
    if (enteredTaskName.trim() === "") {
      setInvalidField(true);
    }
  };

  const onUpdateHandler = async (close) => {
    toaster.success("Your Task Updated", { duration: 1.5 });
    const updatedData = {
      id: id,
      taskName: enteredTaskName || data.taskName,
      description: enteredDes || data.description,
      status: "pending",
    };
    await dispatch(updateTodoData(updatedData, "ashish"));
    dispatch(todoAction.changeDataEvent());
    close();
  };
  return (
    <Pane>
      <Dialog
        isShown={isShown}
        title="Update Todo"
        onCloseComplete={() => close(false)}
        confirmLabel="Confirm"
        onConfirm={onUpdateHandler}
      >
        <Label
          htmlFor="textarea-2"
          marginTop={10}
          marginBottom={4}
          display="block"
        >
          Update Task Name
        </Label>
        <TextInput
          placeholder="Write New Task Name"
          ref={taskNameInputRef}
          isInvalid={invalidField}
          value={enteredTaskName}
          onChange={(event) => {
            setEnteredTaskName(event.target.value);
            setInvalidField(false);
          }}
          onBlur={onBlurHandler}
        />
        <div style={{ fontSize: "12px", marginTop: "10px", color: "red" }}>
          Previous task name : {data.taskName}
        </div>
        <Label
          htmlFor="textarea-2"
          marginTop={10}
          marginBottom={4}
          display="block"
        >
          Update Task Description
        </Label>
        <Textarea
          id="textarea-2"
          placeholder=" Write New Textarea"
          value={enteredDes}
          ref={descriptionRef}
          onChange={(e) => setEnterDes(e.target.value)}
        />
        <div style={{ fontSize: "12px", margin: "10px 0", color: "red" }}>
          Previous description : {data.description}
        </div>
      </Dialog>
    </Pane>
  );
};

export default UpdateTaskInput;
