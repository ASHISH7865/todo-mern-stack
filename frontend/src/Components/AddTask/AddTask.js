import React, { useState, useRef } from "react";
import "./AddTask.scss";
import { Button, AddIcon } from "evergreen-ui";
import TaskInput from "../TaskInput/TaskInput";

const AddTask = () => {
  const [isShown, setIsShown] = useState(false);
  const closeHandler = (payload) => {
    setIsShown(payload);
  };
  return (
    <div className="add-task">
      <Button
        marginY={8}
        marginRight={12}
        iconBefore={AddIcon}
        size="large"
        appearance="primary"
        onClick={() => setIsShown(true)}
      >
        Add New Task
      </Button>
      <TaskInput isShown={isShown} close={closeHandler} />
    </div>
  );
};

export default AddTask;
