import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteTodoData, updateTodoData } from "../../app/todoSlice";
import { todoAction } from "../../app/todoSlice";
import {
  Popover,
  Position,
  Menu,
  PauseIcon,
  TickIcon,
  EditIcon,
  TrashIcon,
  Button,
  Pane,
  Dialog,
  Pill,
  MaximizeIcon,
  PlusIcon,
  Label,
  TextInput,
  Textarea,
} from "evergreen-ui";
import "./TaskCard.scss";

import UpdateTaskInput from "../UpdateTaskInput/UpdateTaskInput";
const TaskCard = (props) => {
  const [isShown, setIsShown] = useState(false);
  const [editDialog, setEditDialog] = useState(false);
  const dispatch = useDispatch();

  const deleteTask = async () => {
    await dispatch(deleteTodoData(props.id));
    dispatch(todoAction.changeDataEvent());
  };

  const editDialogHandler = (payload) => {
    setEditDialog(payload);
  };
  return (
    <>
      <UpdateTaskInput
        isShown={editDialog}
        close={editDialogHandler}
        data={{
          taskName: props.title,
          description: props.description,
        }}
        id={props.id}
      />
      <Pane className="task-column">
        <div className="task-name-wrapper">
          <Pill className="badge" display="inline-flex" color="#ee6c4d">
            {props.pillValue + 1}
          </Pill>
          <h3 className="task-title">{props.title}</h3>
        </div>

        <Dialog
          className="task-dialog"
          isShown={isShown}
          title={props.title}
          onCloseComplete={() => setIsShown(false)}
          hasFooter={false}
        >
          {props.description}
        </Dialog>
      </Pane>
    </>
  );
};

export default TaskCard;
