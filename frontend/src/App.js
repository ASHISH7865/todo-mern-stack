import "./App.scss";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import AddTask from "./Components/AddTask/AddTask";
import Header from "./Components/Header/Header";
import TaskContainer from "./Components/TaskContainer/TaskContainer";
import { fetchTodoData } from "./app/todoSlice";

function App() {
  const loading = useSelector((state) => state.todo.loading) || null;
  const data = useSelector((state) => state.todo.data);

  return (
    <div className="App">
      <Header />
      <AddTask />
      <TaskContainer data={data} loading={loading} />
    </div>
  );
}

export default App;
