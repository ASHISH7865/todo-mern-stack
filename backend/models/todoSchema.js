const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const todoSchema = new Schema({
  taskName: { type: String, required: true },
  description: { type: String },
  status: { type: String },
});

module.exports = mongoose.model("Todo", todoSchema);
