const express = require("express");

const todoController = require("../controller/todo-controller");

const router = express.Router();

router.get("/", todoController.getAllTodo);
router.get("/:tid", todoController.getTodoById);
router.post("/", todoController.createTodo);
router.patch("/:tid", todoController.updateTodo);
router.patch("/status/:tid", todoController.updateTodoStatus);
router.delete("/:tid", todoController.deleteTodo);
module.exports = router;
