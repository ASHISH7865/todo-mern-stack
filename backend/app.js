const express = require("express");
const bodyParser = require("body-parser");
const todoRoutes = require("./routes/todo-routes");
const HttpError = require("./models/http-error");
const mongoose = require("mongoose");

const app = express();
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With,Content-Type,Accept,Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PATCH,DELETE");
  next();
});
app.use("/api/v1/todo", todoRoutes);
app.use((req, res, next) => {
  throw new HttpError("Could not find route", 404);
});
app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({ message: error.message || "AN unknown error occur" });
});

mongoose
  .connect(
    "mongodb+srv://admin:admin123@tododatabase.yjmd8.mongodb.net/todoDatabase?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("connection established");
    app.listen(5000);
  })
  .catch((err) => {
    console.log(err);
  });
