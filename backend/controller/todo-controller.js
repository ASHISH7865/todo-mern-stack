// const { v4: uuidv4 } = require("uuid/v5");
const HttpError = require("../models/http-error");
const Todo = require("../models/todoSchema");
const DUMMY_TODO = [
  {
    id: "1",
    name: "React",
    description: "Today i want to Learn React",
  },
  {
    id: "2",
    name: "React",
    description: "Today i want to Learn React",
  },
];

exports.getTodoById = async (req, res, next) => {
  const tid = req.params.tid;
  let todo;
  try {
    todo = await Todo.findById(tid);
  } catch (err) {
    let error = new HttpError(
      "Something Went wrong ! could not find a todo",
      500
    );
    return next(error);
  }
  if (!todo) {
    const error = new HttpError(
      "Could not find a todo for the provided id",
      400
    );
    return next(error);
  }
  res.json({ todo: todo.toObject({ getters: true }) });
};

exports.getAllTodo = async (req, res, next) => {
  let todo;
  try {
    todo = await Todo.find();
  } catch (err) {
    let error = new HttpError(
      "Something Went wrong ! could not find a todo",
      500
    );
    return next(error);
  }
  if (!todo) {
    const error = new HttpError(
      "Could not find a todo for the provided id",
      400
    );
    return next(error);
  }
  res.json({ todo });
};

exports.createTodo = async (req, res, next) => {
  const { taskName, description, status } = req.body;
  const createdTodo = new Todo({
    taskName,
    description,
    status,
  });
  try {
    await createdTodo.save();
  } catch (err) {
    const error = new HttpError("creating todo failed,Please try again.", 500);
    return next(error);
  }

  res.status(201).json({ todo: createdTodo });
};

exports.updateTodo = async (req, res, next) => {
  const tid = req.params.tid;
  console.log(req.body);
  const { taskName, description, status } = req.body;
  let todo;
  try {
    todo = await Todo.findById(tid);
  } catch (err) {
    let error = new HttpError(
      "Something Went wrong ! could not find a todo",
      500
    );
    return next(error);
  }
  if (!todo) {
    const error = new HttpError(
      "Could not find a todo for the provided id",
      400
    );
    return next(error);
  }

  todo.taskName = taskName;
  todo.status = status;
  if (description) {
    todo.description = description;
  } else {
    todo.description = "";
  }
  try {
    await todo.save();
  } catch (err) {
    const error = new HttpError("updating todo failed,Please try again.", 500);
    return next(error);
  }
  res.status(200).json({ message: "todo updated ." });
};

exports.deleteTodo = async (req, res, next) => {
  const tid = req.params.tid;
  let todo;
  try {
    todo = await Todo.findById(tid);
  } catch (err) {
    let error = new HttpError(
      "Something Went wrong ! could not delete a todo",
      500
    );
    return next(error);
  }

  try {
    await todo.remove();
  } catch (err) {
    const error = new HttpError("something went wrong", 500);
    return next(error);
  }

  res.status(200).json({ message: "Deleted todo." });
};

exports.updateTodoStatus = async (req, res, next) => {
  const tid = req.params.tid;
  console.log(req.body);
  const { status } = req.body;
  let todo;
  try {
    todo = await Todo.findById(tid);
  } catch (err) {
    let error = new HttpError(
      "Something Went wrong ! could not find a todo",
      500
    );
    return next(error);
  }
  if (!todo) {
    const error = new HttpError(
      "Could not find a todo for the provided id",
      400
    );
    return next(error);
  }

  todo.status = status;
  try {
    await todo.save();
  } catch (err) {
    const error = new HttpError(
      "updating todo status failed,Please try again.",
      500
    );
    return next(error);
  }
  res.status(200).json({ message: "todo updated ." });
};
